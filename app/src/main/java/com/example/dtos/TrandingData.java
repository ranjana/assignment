package com.example.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class TrandingData {
       private  String author;
    private  String  name;
    private  String avatar;
    private  String url;
    private  String description;
    private  String language;
    private  String languageColor;
    private  long stars;
    private  long forks;
    private  long currentPeriodStars;
    private ArrayList<UserDetail> builtBy;


    @JsonProperty("builtBy")
    public ArrayList<UserDetail> getBuiltBy() {
        return builtBy;
    }

    public void setBuiltBy(ArrayList<UserDetail> builtBy) {
        this.builtBy = builtBy;
    }

    @JsonProperty("author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty("avatar")
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    @JsonProperty("languageColor")
    public String getLanguageColor() {
        return languageColor;
    }

    public void setLanguageColor(String languageColo) {
        this.languageColor = languageColo;
    }
    @JsonProperty("stars")
    public long getStars() {
        return stars;
    }

    public void setStars(long stars) {
        this.stars = stars;
    }
    @JsonProperty("forks")
    public long getForks() {
        return forks;
    }

    public void setForks(long forks) {
        this.forks = forks;
    }
    @JsonProperty("currentPeriodStars")
    public long getCurrentPeriodStars() {
        return currentPeriodStars;
    }

    public void setCurrentPeriodStars(long currentPeriodStars) {
        this.currentPeriodStars = currentPeriodStars;
    }
}
