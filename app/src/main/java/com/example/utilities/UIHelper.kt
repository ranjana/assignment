package com.example.utilities

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

object UIHelper {
    @JvmStatic
    fun loadImageWithGlide(context: Context,url: String,imageView: ImageView){
        Glide.with(context)
            .load(url)
            .into(imageView);
    }
    fun hideViewWithAnim(exitView: View): ValueAnimator? {
        return hideViewWithAnim(exitView, null)
    }
    fun hideViewWithAnim(exitView: View, animatorListener: Animator.AnimatorListener?): ValueAnimator? {
        exitView.visibility = View.VISIBLE
        val height = exitView.height // UiHelper.getViewHeightBeforeLayout(exitView);
        val params = exitView.layoutParams
        params.height = height
        exitView.layoutParams = params
        val animator = ValueAnimator.ofFloat(0f, 1f).setDuration(300)
        animator.addUpdateListener { animation ->
            val params = exitView.layoutParams
            params.height = ((1 - animation.animatedFraction) * height).toInt()
            exitView.layoutParams = params
        }
        if (animatorListener == null) {
            animator.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {
                    exitView.visibility = View.GONE
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
        } else {
            animator.addListener(animatorListener)
        }
        animator.start()
        return animator
    }

}