package com.example.myapplication

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.dtos.TrandingData
import com.facebook.shimmer.ShimmerFrameLayout
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.no_detail_found.*
import java.io.IOException


class MainActivity : AppCompatActivity() {

    lateinit var linearLayout: LinearLayoutManager
    private var trandingAdapter : TradingDataAdapter? = null
    private val url = "https://ghapi.huchen.dev/repositories?language=&since=daily&spoken_language_code="
    private lateinit var  mShimmerViewContainer: ShimmerFrameLayout

   private lateinit var  mSwipeRefreshLayout : SwipeRefreshLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeView()
        requestDataAndInsertTodataBase()
    }

    private fun initializeView() {
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mSwipeRefreshLayout =  findViewById(R.id.swiperefresh)
        mSwipeRefreshLayout.setOnRefreshListener {
            mShimmerViewContainer.startShimmerAnimation()
            as_recycle_cover.visibility = View.GONE
            requestDataAndInsertTodataBase()
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }


    private fun setData( trandingData: java.util.ArrayList<TrandingData>? = null) {
        linearLayout = LinearLayoutManager(applicationContext)
            .apply {
                orientation = LinearLayoutManager.VERTICAL
            }
        as_recycle_cover.layoutManager = linearLayout
        trandingAdapter = TradingDataAdapter(this,trandingData)
        as_recycle_cover.adapter = trandingAdapter
        retry.setOnClickListener {
            mShimmerViewContainer.startShimmerAnimation()
            requestDataAndInsertTodataBase()
        }

    }
    private fun requestDataAndInsertTodataBase() {
        HttpNetworkCall(url, object : HttpNetworkCall.OnTaskCompleted {
          override fun onSuccessfullCompetion(json: String?) {
              mShimmerViewContainer.stopShimmerAnimation()
              mShimmerViewContainer.visibility = View.GONE
              as_recycle_cover.visibility = View.VISIBLE
              val objectMapper = ObjectMapper()
                try {
                    val value:  ArrayList<TrandingData> = objectMapper.readValue(json, object : TypeReference<List<TrandingData>>() {})
                    setData(value)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure() {
                no_details.visibility = View.VISIBLE
                as_recycle_cover.visibility = View.GONE

            }
        }).execute()
    }

    override fun onPause() {
        mShimmerViewContainer.stopShimmerAnimation()
        as_recycle_cover.visibility = View.VISIBLE
        super.onPause()
    }

}