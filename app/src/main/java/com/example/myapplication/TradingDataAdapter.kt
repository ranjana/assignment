package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.dtos.TrandingData
import com.example.utilities.UIHelper
import kotlinx.android.synthetic.main.trandingitem.view.*

class TradingDataAdapter(var context: AppCompatActivity,var list : List<TrandingData>?) : RecyclerView.Adapter<TradingDataHolder>() {
    var viewList: ArrayList<View> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradingDataHolder {
        return TradingDataHolder(LayoutInflater.from(context).inflate(R.layout.trandingitem, parent, false))

    }

    override fun getItemCount(): Int {
        return list?.size ?: 0

    }


    override fun onBindViewHolder(holder: TradingDataHolder, position: Int) {

        if (!viewList.contains(holder.clTop)) {
            viewList.add(holder.clTop)
        }
        holder.name.text = list?.get(position)?.name?: ""
        holder.tvHeader.text = list?.get(position)?.author?:""
        holder.tvCoverSubText.text = list?.get(position)?.description?:""
        holder.tv_red.text = list?.get(position)?.language?:""
        holder.tv_star.text = list?.get(position)?.stars?.toString()?: ""
        holder.tv_fork.text = list?.get(position)?.forks?.toString()?: ""
        UIHelper.loadImageWithGlide(context,list?.get(position)?.builtBy?.get(0)?.avatar!!,holder.iv_image)
        holder.clTop.setOnClickListener {
            for (item in viewList) {
                holder.handleViewDefault(item)
            }
            holder.handleViewSelected()
//            UIHelper.hideViewWithAnim(holder.tvCoverSubText)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    fun updateCoverAmount(list: List<TrandingData>?){
        this.list = list
        notifyDataSetChanged()
    }
}


class TradingDataHolder(view: View) : RecyclerView.ViewHolder(view) {


    fun handleViewSelected() {
        gpHide.visibility = View.VISIBLE

    }

    fun handleViewDefault(view: View) {
        view.findViewById<androidx.constraintlayout.widget.Group>(R.id.gp_hide).visibility = View.GONE
    }


    val tvHeader= view.tranding_header
    val tvCoverSubText = view.tranding_sub_header
    val clTop = view.cl_top
    val gpHide = view.gp_hide
    val tv_star = view.tv_star
    val tv_red = view.tv_red
    val tv_fork = view.tv_fork
    val name = view.name
    val iv_image = view.iv_benefit



}