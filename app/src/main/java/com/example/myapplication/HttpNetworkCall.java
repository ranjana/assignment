package com.example.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * @author ranjana.rani on 09/09/19.
 */
public class HttpNetworkCall extends AsyncTask<String, String, String> {
    private String line;
    private String jsonData;
    private OnTaskCompleted onTaskCompletedlistener;
    private String url;


    public interface OnTaskCompleted {
        void onSuccessfullCompetion(String json);
        void onFailure();
    }

    public HttpNetworkCall(String url, OnTaskCompleted onTaskCompletedlistener){
        this.url = url;
        this.onTaskCompletedlistener = onTaskCompletedlistener;


    }
    @Override
    protected String doInBackground(String... urls) {
        try {
            URL url = new URL("https://ghapi.huchen.dev/repositories?language=&since=daily&spoken_language_code");
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-length", "0");
            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setConnectTimeout(100000);
            connection.setReadTimeout(100000);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                return sb.toString();
            }
//
//            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            StringBuffer buffer = new StringBuffer();
//
//            while ((line = reader.readLine()) != null) {
//                buffer.append(line);
//
//            }
//            return buffer.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }

    @Override
    protected void onPostExecute(String json) {
        super.onPostExecute(json);
        if(onTaskCompletedlistener != null){
            if(!json.isEmpty()) {
                onTaskCompletedlistener.onSuccessfullCompetion(json);
            }else {
                onTaskCompletedlistener.onFailure();
            }
        }

    }


}
